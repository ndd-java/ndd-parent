# How to release a NDD project ?

Push all sources to origin

Start a GitFlow release

Update the POM with release versions (__parent and dependencies__)

Build the artifacts by running:

```bash
pushd "$NDD_DIR/$project/target"
mvn clean install gpg:sign site
popd
```

Build the artifacts bundle:

```bash
pushd "$NDD_DIR/$project/target"
jar -cvf bundle.jar $project-*
popd
```

Commit the POM with the release version

Finish the GitFlow release

Update the site:

```bash
rm -rf $NDD_DIR/ndd-java.bitbucket.org/$project/*
cp -r $NDD_DIR/$project/target/site/* $NDD_DIR/ndd-java.bitbucket.org/$project/
```

Commit the site `ndd-java.bitbucket.org`

Go to [Sonatype repository](https://oss.sonatype.org) then:

* Build promotion > Staging upload: `upload mode` = `artifact bundle`
* Build promotion > Staging repositories: release `namedidierdavid`
    
Update the POM with SNAPSHOT versions

Commit the POM
