#!/bin/bash

NDD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}")/.." && pwd )"

function ndd-build() {
    projects=();
    projects+=('ndd-parent')
    projects+=('ndd-build')
    projects+=('ndd-parent-java')
    projects+=('ndd-check4j')
    projects+=('ndd-test4j')
    projects+=('ndd-utils')
    projects+=('ndd-ansible4j')
    projects+=('ndd-storm')

    for project in "${projects[@]}"; do
        pushd "$project" > /dev/null

        mvn $*
        mvn_result=$?

        popd > /dev/null

        [ "$mvn_result" == "0" ] || break
    done
}

ndd-build $*
