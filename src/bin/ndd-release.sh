#!/bin/bash

NDD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}")/.." && pwd )"

NDD_PROJECTS=();
NDD_PROJECTS+=('ndd-parent')
NDD_PROJECTS+=('ndd-build')
NDD_PROJECTS+=('ndd-parent-java')
NDD_PROJECTS+=('ndd-check4j')
NDD_PROJECTS+=('ndd-test4j')
NDD_PROJECTS+=('ndd-utils')
NDD_PROJECTS+=('ndd-ansible4j')
NDD_PROJECTS+=('ndd-storm-test')
NDD_PROJECTS+=('ndd-storm-test;ndd-storm/ndd-storm-test')
NDD_PROJECTS+=('ndd-storm-mongodb;ndd-storm/ndd-storm-mongodb')

TEXT_RESET="\e[0m"
TEXT_BOLD="\e[1m"
TEXT_UNDERLINED="\e[4m"
TEXT_RESET_BOLD="\e[21m"
TEXT_RESET_UNDERLINED="\e[24m"

TEXT_FG_RED="\e[31m"
TEXT_FG_GREEN="\e[32m"
TEXT_FG_YELLOW="\e[33m"

COLOR_HEADER="$TEXT_BOLD$FG_GREEN"
COLOR_STEP="$TEXT_BOLD$FG_YELLOW"



function ndd-release() {
    if [ $# -eq 0 ]; then
        _ndd-release_usage-error "No argument provided"
    fi

    case $1 in
        -a|--all)    _ndd-release_all    ${@:2} ;;
        -h|--help)   _ndd-release_usage         ;;
        *)           _ndd-release_list   ${@:1} ;;
    esac
}



function _ndd-release_usage() {
    echo "Usage:"
    echo "  ndd-release <project> [more_projects]"
    echo "  ndd-release [-a|--all]"
    echo "  ndd-release [-h|--help]"
}

function _ndd-release_usage-error() {
    echo -e "${TEXT_FG_RED}ERROR: $1${TEXT_RESET}"
    _ndd-release_usage
    exit -1
}




function _ndd-release_print-header() {
    echo
    echo -e "${TEXT_FG_GREEN}********** ${1}${TEXT_RESET}"
    echo
}

function _ndd-release_print-step() {
    echo -e "${TEXT_FG_YELLOW}${step_counter}. ${1}${TEXT_RESET}" && read
    step_counter=$((step_counter+1))
}

function _ndd-release_print-step_by() {
    _ndd-release_print-step "${1}, by pressing [Enter]"
}

function _ndd-release_print-step_then() {
    _ndd-release_print-step "${1}, then press [Enter]"
}



function _ndd-release_all() {
    _ndd-release_list ${NDD_PROJECTS[@]}
}

function _ndd-release_list() {
    local projects=("${@}")
    local step_counter=1

    _ndd-release_print-header "Releasing the following projects"

    for project in "${projects[@]}"; do
        IFS=';' read -a project_array <<< "$project"
        local project_name="${project_array[0]}"
        local project_path="$project_name"

        if [ ${#project_array[@]} -eq 2 ]; then
            project_path="${project_array[1]}"
        fi

        printf "  - %s at ./%s\n" "$project_name" "$project_path"
    done

    for project in "${projects[@]}"; do
        IFS=';' read -a project_array <<< "$project"
        local project_name="${project_array[0]}"
        local project_path="$project_name"

        if [ ${#project_array[@]} -eq 2 ]; then
            project_path="${project_array[1]}"
        fi

        _ndd-release_single $project_name $project_path
    done
}

function _ndd-release_single() {
    local project_name="$1"
    local project_path="$2"

    if [ -z "$project_name" ]; then
        _ndd-release_usage-error "No project name specified"
    fi
    if [ -z "$project_path" ]; then
        _ndd-release_usage-error "No project path specified"
    fi

    _ndd-release_print-header "Now releasing project '$project_name' at '$project_path'"

    _ndd-release_print-step_then "[$project_name] Push all sources to origin"
    _ndd-release_print-step_then "[$project_name] Start a GitFlow release"
    _ndd-release_print-step_then "[$project_name] Update the POM with release versions (${TEXT_UNDERLINED}parent and dependencies${TEXT_RESET_UNDERLINED})"
    _ndd-release_print-step_then "[$project_name] Update the changelog"
    _ndd-release_print-step_by   "[$project_name] Build the artifacts"

    pushd "$NDD_DIR/$project_path" > /dev/null
    mvn -N clean install gpg:sign site
    mvn_result=$?
    popd > /dev/null
    [ "$mvn_result" == "0" ] || exit 1

    echo
    _ndd-release_print-step_by "[$project_name] Build the artifacts bundle"

    pushd "$NDD_DIR/$project_path/target" > /dev/null
    jar -cvf bundle.jar $project_name-*
    popd > /dev/null

    echo
    _ndd-release_print-step_then "[$project_name] Commit the POM with the release version"
    _ndd-release_print-step_then "[$project_name] Finish the GitFlow release"
    _ndd-release_print-step_then "[$project_name] Move the tag if necessary"
    _ndd-release_print-step_by   "[$project_name] Update the site"

    rm -rf $NDD_DIR/ndd-java.bitbucket.org/$project_name/*
    cp -r $NDD_DIR/$project_path/target/site/* $NDD_DIR/ndd-java.bitbucket.org/$project_name/

    echo
    _ndd-release_print-step_then "[$project_name] Commit and push the site (ndd-java.bitbucket.org)"

    _ndd-release_print-step_then "[$project_name] Go to Sonatype repository (https://oss.sonatype.org)"
    _ndd-release_print-step_then "[$project_name] Build promotion > Staging upload: upload mode = artifact bundle"
    _ndd-release_print-step_then "[$project_name] Build promotion > Staging repositories: release namedidierdavid"

    _ndd-release_print-step_then "[$project_name] Update the POM with SNAPSHOT versions"
    _ndd-release_print-step_then "[$project_name] Update the changelog"
    _ndd-release_print-step_then "[$project_name] Commit and push the POM"

    _ndd-release_print-header "Released project '$project_name'"
}



ndd-release $*
