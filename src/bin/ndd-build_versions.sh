#!/bin/bash

NDD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}")/.." && pwd )"

./bin/ndd-build.sh versions:display-plugin-updates versions:display-dependency-updates
