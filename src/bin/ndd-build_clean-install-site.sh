#!/bin/bash

NDD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}")/.." && pwd )"

./bin/ndd-build.sh clean install site
