# NDD Parent

## Version 0.2.4

- [DOCUMENTATION] Update site
- [DEPENDENCY] Update plugins versions and Maven version prerequisite
- [DEPENDENCY] Update Docker Maven plugin
- [DEPENDENCY] Add FailSafe Maven plugin
- [DOCUMENTATION] Update NDD images
- [DEPENDENCY] Add Docker Maven plugin
- [BUILD] Fixed the helper scripts
- [QUALITY] Reformatted the POM

## Version 0.2.3

- [BUILD] Updated release script
- [BUILD] Updated helper scripts
- [BUILD] Added Maven version prerequisite
- [DOCUMENTATION] Added `sites.html` file

## Version 0.2.2

- [DOCUMENTATION] Updated the release documentation
- [DOCUMENTATION] Updated the helper script
- [DOCUMENTATION] Added this changelog

## Version 0.2.1

- [BUILD] Refactored and colorized the release helper script

## Version 0.2.0

- [BUILD] Excluded the Maven dependency plugin from Eclipse
- [BUILD] Excluded the Maven JaCoCo plugin from Eclipse
- [BUILD] Split this project with the new project `ndd-parent-java`
- [BUILD] Added Maven helper scripts to build all the NDD projects
- [BUILD] Added Maven helper scripts to release all the NDD projects
- [DEPENDENCY] Updated version of Apache Commons Lang 3 to 3.3.2

## Version 0.1.2

- [BUILD] Normalized TestNG suite files
- [DEPENDENCY] Added SLF4J and Logback

## Version 0.1.1

- [DEPENDENCY] Added Mockito
- [DEPENDENCY] Added Apache Commons Lang 3

## Version 0.1.0

- Initial commit
